package main

import (
	"bufio"
	"flag"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/mleykum/example-project/notifier"
)

var url = flag.String("url", "", "URL the notification messages will be POSTed to")
var interval = flag.Duration("interval", 5*time.Second, "Notification interval")
var conn = flag.Int("connections", 10, "Maximum number of parallel connections to the server")
var debug = flag.Bool("debug", false, "Print information about all notifications sent")

func main() {
	flag.Parse()

	if *url == "" {
		flag.Usage()
		os.Exit(1)
	}

	// create Notifier instance
	n, err := notifier.NewNotifier(*url, *conn)

	if err != nil {
		log.Printf("ERROR: Could not create Notifier: %s", err)
		os.Exit(1)
	}

	defer n.Close() // clean up and send remaining queued messages before terminating

	// watch for SIGINT
	chSigint := make(chan (os.Signal), 1)
	signal.Notify(chSigint, os.Interrupt)

	chStop := make(chan (bool), 1) // channel to make processing loop stop sending messages
	chDone := make(chan (bool), 1) // channel to be notified when processing loop is done

	// start processing loop as a goroutine
	go readAndPostMessages(n, *interval, chStop, chDone)

	// wait for either SIGINT (chSigint) or EOF (chDone) before terminating
	select {
	case <-chSigint:
		chStop <- true // do not send further messages
		log.Printf("SIGINT received, will terminate after queue has finished processing")
	case <-chDone:
	}
}

// readAndPostMessages reads text from STDIN and posts each line to the
// notification server, until EOF is reached or a signal in chStop is received.
// When done reading, a signal to chDone is sent.
func readAndPostMessages(n *notifier.Notifier, t time.Duration, chStop chan (bool), chDone chan (bool)) {

	s := bufio.NewScanner(os.Stdin)

	cnt := 0

	for s.Scan() {
		// Check if we need to stop sending messages (SIGINT received)
		select {
		case <-chStop:
			return
		default:
		}

		// read a line and send it as a notification
		line := s.Text()
		err := n.SendMessage(line, generateErrorHandler(cnt))
		if err != nil {
			log.Printf("ERROR: Could not send message: %s", err)
		} else if *debug {
			log.Printf("DEBUG: Sent message '%s'", line)
		}

		cnt++

		time.Sleep(t)
	}

	if err := s.Err(); err != nil {
		log.Printf("ERROR: Could not read from STDIN: %s", err)
	}

	chDone <- true
}

// generateErrorHandler creates an error handling callback function with
// the local line number as additional context
func generateErrorHandler(num int) notifier.ErrorHandlerCallback {
	return func(err error, msg string) { errorHandler(err, msg, num) }
}

// errorHandler logs error details
func errorHandler(err error, msg string, num int) {
	log.Printf("ERROR: %s while trying to post notification %d: %s", err, num, msg)
}
