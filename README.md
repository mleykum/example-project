# A small example project

I'm afraid I don't really have any code I could publicly share, but if you want
to take a look at a small reference project, this is my implementation of a
task set for an earlier job interview.

The task was to create a client library for a notification server that will
accept messages to be posted to a HTTP server in an efficient, non-blocking way,
and an executable that will use this library to post messages read from stdin
to a server with a given delay between messages.