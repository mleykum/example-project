package notifier

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// TestNewNotifier tests the initialisation of a Notifier
func TestNewNotifier(t *testing.T) {
	testdata := []struct {
		testName     string
		serverUrl    string
		maxConn      int
		wantError    bool
		wantErrorMsg string
	}{
		{
			testName:  "Complete initialisation",
			serverUrl: "http://www.example.com/",
			maxConn:   5,
			wantError: false,
		},
		{
			testName:     "Invalid URL 1",
			serverUrl:    "Hello World",
			maxConn:      1,
			wantError:    true,
			wantErrorMsg: "Invalid URL",
		},
		{
			testName:     "Invalid URL 2",
			serverUrl:    "ftp://ftp.example.com/",
			maxConn:      1,
			wantError:    true,
			wantErrorMsg: "Invalid URL",
		},
		{
			testName:     "Invalid maxConn 1",
			serverUrl:    "http://www.example.com/notifier/",
			maxConn:      0,
			wantError:    true,
			wantErrorMsg: "Invalid value for maxConn",
		},
		{
			testName:     "Invalid maxConn 2",
			serverUrl:    "http://www.example.com/notifier/",
			maxConn:      -1,
			wantError:    true,
			wantErrorMsg: "Invalid value for maxConn",
		},
	}

	for _, test := range testdata {
		n, err := NewNotifier(test.serverUrl, test.maxConn)

		assert.Equal(t, test.wantError, err != nil, fmt.Sprintf("Test '%s', Error status as expected?", test.testName))
		assert.Equal(t, test.wantError, n == nil, fmt.Sprintf("Test '%s', Notifier as expected?", test.testName))

		if test.wantError && err != nil { // we already checked if err != nil is as expected
			assert.Equal(t, test.wantErrorMsg, err.Error(), fmt.Sprintf("Test '%s', Error message as expected?", test.testName))
		}

		if n != nil {
			assert.Nil(t, n.Close(), fmt.Sprintf("Test '%s', Notifier closed without error?", test.testName))
		}
	}
}

// TestSendMessage tests sending a message to the queue channel
func TestSendMessage(t *testing.T) {
	testdata := []struct {
		testName    string
		notifier    *Notifier
		message     string
		errCallback ErrorHandlerCallback
		nextId      uint64
		wantError   error
		wantId      uint64
	}{
		{
			testName: "All fine",
			notifier: &Notifier{
				active: func() *int32 { i := int32(1); return &i }(),
				queue:  make(chan (message), 1),
			},
			message:     "Test Notification",
			errCallback: func(error, string) {},
			wantError:   nil,
		},
		{
			testName: "All fine, no error callback",
			notifier: &Notifier{
				active: func() *int32 { i := int32(1); return &i }(),
				queue:  make(chan (message), 1),
			},
			message:   "Test Notification",
			wantError: nil,
		},
		{
			testName: "Inactive Notifier 1",
			notifier: &Notifier{
				active: nil,
			},
			message:   "Test Notification",
			wantError: ErrorInactive,
		},
		{
			testName: "Inactive Notifier 2",
			notifier: &Notifier{
				active: func() *int32 { i := int32(0); return &i }(),
			},
			message:   "Test Notification",
			wantError: ErrorInactive,
			wantId:    0,
		},
	}

	for _, test := range testdata {
		err := test.notifier.SendMessage(test.message, test.errCallback)

		assert.Equal(t, test.wantError, err, fmt.Sprintf("Test '%s', Error as expected?", test.testName))

		time.Sleep(100 * time.Millisecond) // wait a bit to let the goroutine finish

		if test.wantError == nil {
			assert.Equal(t, 1, len(test.notifier.queue), fmt.Sprintf("Test '%s', Queue size as expected?", test.testName))
			if len(test.notifier.queue) > 0 {
				msg := <-test.notifier.queue
				assert.Equal(t, test.message, msg.msg, fmt.Sprintf("Test '%s', Message as expected?", test.testName))
				assert.Equal(t, test.errCallback != nil, msg.errCallback != nil, fmt.Sprintf("Test '%s', Message errCallback set as expected?", test.testName))
			}
		}
	}
}

// TestWorker tests the process from taking a message from the queue channel
// to it being posted to the http server.
func TestWorker(t *testing.T) {
	// restore overwritten function pointer after test
	defer func(f func(string, string, io.Reader) (*http.Response, error)) { httpPost = f }(httpPost)

	// simple mock for http.Post
	httpPost = func(url, contentType string, body io.Reader) (*http.Response, error) {
		b, err := ioutil.ReadAll(body)
		if err != nil {
			return nil, err
		}
		key := fmt.Sprintf("%s-%s-%s", url, contentType, string(b))

		data := map[string]struct {
			resp *http.Response
			err  error
		}{
			"http://www.example.com/-text/plain-Test Message": {
				resp: &http.Response{
					Body:       ioutil.NopCloser(strings.NewReader("")),
					Status:     "200 OK",
					StatusCode: 200,
				},
				err: nil,
			},
			"http://www.example.com/-text/plain-BREAK THE SERVER": {
				resp: &http.Response{
					Body:       ioutil.NopCloser(strings.NewReader("")),
					Status:     "500 Internal Server Error",
					StatusCode: 500,
				},
				err: nil,
			},
			"http://www.inva.lid-text/plain-Test Message": {
				resp: nil,
				err:  errors.New("no such host"),
			},
		}

		if result, ok := data[key]; ok {
			return result.resp, result.err
		}

		return nil, fmt.Errorf("No mock data for key %s", key)
	}

	var cbMutex sync.Mutex
	var cbErr []error
	var cbMsg []string

	var testCallback = func(err error, msg string) {
		cbMutex.Lock()
		defer cbMutex.Unlock()
		cbErr = append(cbErr, err)
		cbMsg = append(cbMsg, msg)
	}

	testdata := []struct {
		testName  string
		url       string
		queue     []message
		wantCbErr []error
		wantCbMsg []string
	}{
		{
			testName:  "One successful post, one server error",
			url:       "http://www.example.com/",
			queue:     []message{{msg: "Test Message", errCallback: testCallback}, {msg: "BREAK THE SERVER", errCallback: testCallback}},
			wantCbErr: []error{errors.New("Unexpected HTTP response status: 500 Internal Server Error")},
			wantCbMsg: []string{"BREAK THE SERVER"},
		},
		{
			testName:  "HTTP request fails",
			url:       "http://www.inva.lid",
			queue:     []message{{msg: "Test Message", errCallback: testCallback}},
			wantCbErr: []error{errors.New("no such host")},
			wantCbMsg: []string{"Test Message"},
		},
		{
			testName:  "HTTP request fails, no error handler",
			url:       "http://www.inva.lid",
			queue:     []message{{msg: "Test Message"}},
			wantCbErr: []error{},
			wantCbMsg: []string{},
		},
	}

	for _, test := range testdata {
		for mode := 0; mode <= 2; mode++ {
			cbMutex.Lock()
			cbErr = []error{}
			cbMsg = []string{}
			cbMutex.Unlock()

			n := &Notifier{
				active:    func() *int32 { i := int32(1); return &i }(),
				queue:     make(chan (message), 10),
				wgWorkers: sync.WaitGroup{},
				url:       test.url,
			}

			switch mode {
			case 0: // Test mode 0: Items are added with some delay while worker is running
				n.wgWorkers.Add(1)
				go n.worker()

				for _, m := range test.queue {
					n.queue <- m
					time.Sleep(100 * time.Millisecond)
				}

				atomic.StoreInt32(n.active, 0) // set inactive to stop worker
			case 1: // Test mode 1: Multiple items still in queue when Notifier closed
				for _, m := range test.queue {
					n.queue <- m
				}
				atomic.StoreInt32(n.active, 0) // set inactive
				n.wgWorkers.Add(1)
				n.worker()
			case 2: // Test mode 2: Concurrent workers
				n.wgWorkers.Add(2)
				go n.worker()
				go n.worker()

				for _, m := range test.queue {
					n.queue <- m
				}

				atomic.StoreInt32(n.active, 0) // set inactive to stop workers
			default:
				t.Fatalf("Invalid test mode: %d", mode)
			}

			n.wgWorkers.Wait() // if this blocks, something is broken

			assert.Equal(t, 0, len(n.queue), fmt.Sprintf("Test '%s', queue channel size as expected?", test.testName))

			cbMutex.Lock()
			assert.Equal(t, len(test.wantCbErr), len(cbErr), fmt.Sprintf("Test '%s', number of errors as expected?", test.testName))
			assert.Equal(t, len(test.wantCbMsg), len(cbMsg), fmt.Sprintf("Test '%s', number of messages as expected?", test.testName))

			for id, err := range test.wantCbErr {
				assert.Equal(t, err, cbErr[id], fmt.Sprintf("Test '%s', error for %d as expected?", test.testName, id))
			}
			for id, msg := range test.wantCbMsg {
				assert.Equal(t, msg, cbMsg[id], fmt.Sprintf("Test '%s', message for %d as expected?", test.testName, id))
			}
			cbMutex.Unlock()
		}
	}
}

// TestClose tests closing a Notifier
func TestClose(t *testing.T) {
	n := &Notifier{
		active:    func() *int32 { i := int32(1); return &i }(),
		queue:     make(chan (message), 1),
		wgWorkers: sync.WaitGroup{},
	}
	err := n.Close()
	assert.Equal(t, false, n.isActive(), "Notifier still active?")
	assert.Nil(t, err, "Closed without error?")

	err = n.Close()
	assert.Equal(t, ErrorInactive, err, "Error when closing closed Notifier")

	n = &Notifier{}
	err = n.Close()
	assert.Equal(t, ErrorInactive, err, "Error when closing uninitialised Notifier")
}

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}
