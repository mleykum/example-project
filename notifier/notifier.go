package notifier

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"sync/atomic"
)

// Notifier provides the functionality to asynchronously send text
// notifications to a HTTP server.
type Notifier struct {
	url         string               // HTTP server (POST destination) URL
	errCallback ErrorHandlerCallback // Callback in case of errors, can be nil
	queue       chan (message)       // Message queue channel
	wgWorkers   sync.WaitGroup       // Wait group to make sure all workers are done when closing the Notifier
	active      *int32               // Is Notifier intialised and running? (0/1)
}

// ErrorHandlerCallback is the function interface for a callback that will
// receive any error that occurred while posting a notification message.
// The error and the concerned notification message will be passed.
type ErrorHandlerCallback func(error, string)

// message is an internal wrapper for keeping a message and its error handler together
type message struct {
	msg         string
	errCallback ErrorHandlerCallback
}

// ErrorInactive will be returned when trying to send notifications using
// a Notifier that hasn't been initialised or that has been closed.
var ErrorInactive = errors.New("Notifier is inactive (not initialised or closed)")

// NewNotifier creates a new Notifier instance with the passed destination URL
// and number of maximum parallel connections.
// It will return an error if any of the parameters are invalid.
func NewNotifier(serverUrl string, maxConn int) (*Notifier, error) {
	// validate parameters
	u, err := url.Parse(serverUrl)
	if err != nil || u.Host == "" || (u.Scheme != "http" && u.Scheme != "https") {
		return nil, errors.New("Invalid URL")
	}
	if maxConn <= 0 {
		return nil, errors.New("Invalid value for maxConn")
	}

	// init Notifier struct
	queue := make(chan (message), 1000) // Assume 1000 is a reasonable size.
	// See comment in SendMessage below for more context.

	n := &Notifier{
		url:    serverUrl,
		queue:  queue,
		active: func() *int32 { i := int32(1); return &i }(),
	}

	// start workers
	for i := 0; i < maxConn; i++ {
		n.wgWorkers.Add(1)
		go n.worker()
	}

	return n, nil
}

// SendMessage sends a notification message into the queue to be posted to
// the HTTP server. This never blocks.
// If errCallback is not nil, this will be called if an error occurs while
// trying to post the message.
func (n *Notifier) SendMessage(msg string, errCallback ErrorHandlerCallback) error {
	if !n.isActive() { // check if Notifier has been initialised and isn't closed
		return ErrorInactive
	}

	// start a goroutine to queue the message
	go n.queueMessage(message{msg: msg, errCallback: errCallback})

	// The idea here is to make sure the call never blocks while also keeping
	// the message channel to a reasonable size. If the channel size is
	// exhausted, it still wouldn't block the caller as the goroutines will
	// wait in the background for channel space to become available.
	// In a real-world application, there would probably be better options
	// depending on the context and expected usage, e.g. blocking the caller
	// if the channel is full, returning errors/discarding messages when
	// over capacity, creating a large-enough channel, ...

	return nil
}

// Close stops the notifier. All currently queued messages will still be posted,
// but no new messages will be accepted, and all worker goroutines will
// terminate. This blocks until the remaining queue has been processed.
func (n *Notifier) Close() error {
	if !n.isActive() {
		return ErrorInactive
	}
	atomic.StoreInt32(n.active, 0) // set as inactive, so no new messages will be accepted

	// (In theory, if Close and SendMessage are called at the same time while
	// the queue is empty, an accepted message might go missing.
	// But as this would only happen in a context that doesn't really make sense
	// (sending and closing concurrently would lead to errors anyway), I won't
	// handle this case.)

	n.wgWorkers.Wait() // wait for all workers to finish

	close(n.queue)

	return nil
}

// isActive is a thread-safe way of checking if the Notifier is initialised
// and not closed
func (n *Notifier) isActive() bool {
	if n.active == nil {
		return false
	}

	active := atomic.LoadInt32(n.active)
	return (active == 1)
}

func (n *Notifier) queueMessage(msg message) {
	n.queue <- msg // add message to queue channel
}

// worker checks the notification queue and starts processing of messages and
// will terminate once the Notifier is inactive and there are no more unprocessed
// messages in the queue
func (n *Notifier) worker() {
	for n.isActive() || len(n.queue) > 0 { // while Notifier is active or there are still messages left...
		select {
		case msg := <-n.queue: // get message from queue and process it
			n.handleMessage(msg)
		default:
		}
	}
	n.wgWorkers.Done()
}

func (n *Notifier) handleMessage(msg message) {
	err := n.postMessage(msg) // try to post the message to the HTTP server
	if err != nil && msg.errCallback != nil {
		// Call error handling callback if there is one and the request was unsuccesful.
		// (Could be called as a goroutine, to make sure a blocking callback won't
		// block the worker - but then more synchronisation effort is needed to
		// make sure all errors have been received when terminating.)
		msg.errCallback(err, msg.msg)
	}
}

var httpPost = http.Post // can be replaced in tests

// postMessage sends the POST request to the HTTP server
func (n *Notifier) postMessage(msg message) error {
	resp, err := httpPost(n.url, "text/plain", strings.NewReader(msg.msg))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Accept all 2xx status codes as success (200 OK or 204 No Content would be likely)
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("Unexpected HTTP response status: %s", resp.Status)
	}

	return nil
}
